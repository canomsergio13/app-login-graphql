import { Inter } from 'next/font/google'
import '../styles/globals.css'
import { ApolloProvider } from '../Providers/ApolloProvider'
import { ChakraProvider__ } from '../Providers/ChakraProvider'

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
	title: 'Iniciar sesión - Cuidado con el Perro',
	description: 'App login para iniciar sesión en Cuidado con el Perro',
}

export default function RootLayout({ children }) {
	return (
		<html lang="es-MX">
			<body className={inter.className}>
				<ApolloProvider> {/* Provider de Apollo */}
					<ChakraProvider__>	{/* Provider de Chakra UI */}
						{children}
					</ChakraProvider__>
				</ApolloProvider>
			</body>
		</html>
	)
}
