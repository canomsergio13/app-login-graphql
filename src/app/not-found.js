import Link from 'next/link'
import React from 'react'

//componente custom de pagina 404
const Custon404 = () => 

    <div 
        className="flex items-center justify-center min-h-screen bg-cover bg-center bg-no-repeat"
        style={{ backgroundImage: "url('https://source.unsplash.com/random/1920x1080?dog')" }}
    >
        <div className="max-w-md mx-auto text-center bg-white bg-opacity-90 p-8 rounded-lg shadow-lg">
            <div className="text-9xl font-bold text-red-600 mb-4">
                404
            </div>
            <h1 className="text-4xl font-bold text-gray-800 mb-6">Página no encontrada</h1>
            <p className="text-lg text-gray-600 mb-8">
                La página al cual intentas acceder no existe.
            </p>
            <Link
                href={'/'}
                className='inline-block bg-red-600 text-white font-semibold px-6 py-3 rounded-md hover:bg-red-700 transition-colors duration-300'
            >
                ir al login
            </Link>
        </div>
    </div>


export default Custon404