import { NextResponse } from "next/server";
import { generateTokenMutation } from "../../../graphql/generateDataServer";

//Api para usar GraphQL desde el servidor
export async function POST(request) {
    try {
        const { email, password } = await request.json();
    
        const token = await generateTokenMutation({
            email, 
            password
        });
    
        return NextResponse.json({
            result: token
        })
    } catch (error) {
        return NextResponse.json({
            error
        })
    }
}
