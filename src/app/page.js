"use client"
import React from 'react'
import Form from '../components/Form'
import { isValidEmail } from '../utils/email'
import { useToast } from '@chakra-ui/react'
import { generate_Token } from '../services/generateToken/index'

const Login = () => {
    const toast = useToast()

    const handleLoginClient = async ({email,password}) => {

        //mandamos a llamar a la funcion de la api
        const response = await generate_Token({email,password})

        //mostramos el Toast Correspodiente
        if(response?.result?.token){
            toast({
                title: 'Token generado exitosamente',
                description: response?.result?.token,
                status: 'success',
                duration: 7000,
                isClosable: true,
            })
            return
        } else if(response?.result?.message){
            toast({
                title: 'El Token no se generó',
                description: response?.result?.message,
                status: 'error',
                duration: 7000,
                isClosable: true,
            })
        }

    }

    //campos de nuestro formulario
    const loginFields = [
        {
            field: "email",
            placeholder: "correo@mail.com",
            title: "Correo",
        },
        {
            field: "password",
            placeholder: "Mi contraseña",
            title: "Contraseña"
        }
    ]

    //valor inicial de nuestro formulario
    const initialFields = {
        email: '',
        password: ''
    };

    //funcion para validar nuestro formulario
    const validateLogin = fields => {
        let errors = "";

        if (!fields.email) {
            errors = 'Por favor ingresa tu correo.';
        } else if (!isValidEmail(fields.email)) {
            errors = 'Por favor ingresa un correo electrónico válido.';
        }

        if (!fields.password) {
            errors += ' Por favor ingresa tu contraseña.';
        }

        return errors;
    };

    //props para nuestro componente Form
    const propsForm ={
        loginFields,
        initialFields,
        validation: validateLogin,
        action: data => handleLoginClient(data)
    }

    return (
        <div >
            <div className="bg-white dark:bg-gray-900">
                <div className="flex justify-center h-screen">
                    <div className="hidden bg-cover lg:block lg:w-2/3" style= {{ backgroundImage: 'url(https://solesta.mx/almacenamiento/locales/cuidado_con_el_perro/banner-principal.jpg)' }}>
                        <div className="flex items-center h-full px-20 bg-gray-900 bg-opacity-40">
                            <div>
                                <h2 className="text-4xl font-bold text-white">
                                    Cuidado con el Perro
                                </h2>
                                
                                <p className="max-w-xl mt-5 text-gray-300">
                                    Descubre las mejores prendas de ropa en México. Encuentra los mejores outfits para mujer, hombre y niños solo en Cuidado Con el Perro en linea.
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    <div className="flex items-center w-full max-w-md px-6 mx-auto lg:w-2/6">
                        <div className="flex-1">
                            <div className="text-center">
                                <h2 className="text-4xl font-bold text-center text-gray-700 dark:text-white">
                                    Inicia sesión
                                </h2>                   
                            </div>
        
                            <Form {...propsForm} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login