
import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';
import { registerApolloClient } from '@apollo/experimental-nextjs-app-support/rsc';

// fn para usar GraphQL en un componente de servidor
export const { getClient } = registerApolloClient(()=> {
    return new ApolloClient({
        cache: new InMemoryCache(),
        link: new HttpLink({
            uri:  process.env.URI,
            headers: {
                Store: process.env.STORE
            }
        })
    })
})