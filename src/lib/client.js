import { ApolloLink, HttpLink } from '@apollo/client'
import { 
    NextSSRApolloClient, 
    NextSSRInMemoryCache, 
    SSRMultipartLink 
} from '@apollo/experimental-nextjs-app-support/ssr';

// fn para usar GraphQL en un componente de cliente
export const makeClient = () => {
    
    const httpLink = new HttpLink({
        uri:  process.env.URI,
        headers: {
            Store: process.env.STORE
        }
    });

    const NextSSRApollo = new NextSSRApolloClient({
        cache: new NextSSRInMemoryCache(),
        link: typeof window === undefined
                ? ApolloLink.from([
                        new SSRMultipartLink({
                            stripDefer: true,
                        }),
                        httpLink
                    ])
                : httpLink
    }) 

    return NextSSRApollo;

}
