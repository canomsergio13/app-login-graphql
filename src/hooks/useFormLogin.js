"use client"
import { useState } from 'react';
import { useToast } from '@chakra-ui/react'

export const useFormLogin = ({initialFields, validation,action,setLoading}) => {
    const toast = useToast()
    const [fields, setFields] = useState(initialFields);

    const handleChange = e => {
        const { name, value } = e.target;
        setFields(prevFields => ({
            ...prevFields,
            [name]: value
        }));
    };

    const handleSubmit = e => {
        e.preventDefault();
        setLoading(true)

        const validationErrors = validation(fields);
        if (validationErrors !== "") {
            toast({
                title: 'Campos no válidos.',
                description: validationErrors,
                status: 'error',
                duration: 7000,
                isClosable: true,
            })
            setLoading(false)
            return;
        }

        action(fields)
        setLoading(false)
        
    };

    return { fields, handleChange, handleSubmit };
};