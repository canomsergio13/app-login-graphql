"use client"
import React, { useState } from 'react'
import { useFormLogin } from '../hooks/useFormLogin'

const Form =  ({ initialFields, validation, loginFields, action }) => {

    const [isChecked, setIsChecked] = useState(false);

    const toggleCheckbox = () =>  setIsChecked(!isChecked);
    const [Loading, setLoading] = useState(false);
    
    //uso de customHook para manejar los cambios
    const { fields, handleChange, handleSubmit } = useFormLogin({
        initialFields, 
        validation,
        setLoading,
        action
    });

    const getType = type => {

        if(type === "password" && isChecked){
            return "text"
        }

        if(type === "password" && !isChecked){
            return "password"
        }

        return type
    }

    return (
        <div>
            {   /* mostramos nuestros campos a usar */
                loginFields.map( f => 
                    <div key={f.field} className="mt-6">
                        <label 
                            htmlFor={f.field} 
                            className="block mb-2 text-sm text-gray-600 dark:text-gray-200"
                        >
                            {f.title}
                        </label>
                        <input
                            type={getType(f.field)}
                            name={f.field}
                            id={f.field}
                            onChange={handleChange}
                            placeholder={f.placeholder}
                            value={fields.field}
                            className='block w-full px-4 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-red-500 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-400 dark:focus:border-blue-400 focus:ring-blue-400 focus:outline-none focus:ring focus:ring-opacity-40'
                        />
                        
                        {
                            f.field === "password" && (
                                <div className="mt-5">
                                    <label className="container">
                                        <input 
                                            type="checkbox"
                                            checked={isChecked}
                                            onChange={toggleCheckbox}
                                         /> 
                                        <span className='show__pass'>
                                            { isChecked ? "Ocultar " : "Mostrar " }contraseña
                                        </span>
                                    </label>
                                </div>
                            )
                        } 
                        
                    </div>  
                )
            }
            <div className="mt-6">
                <button
                    onClick={handleSubmit}
                    type="button"
                    className="w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-blue-500 rounded-md hover:bg-blue-400 focus:outline-none focus:bg-blue-400 focus:ring focus:ring-blue-300 focus:ring-opacity-50"
                >
                {
                    Loading ? "Cargando..." : "Entrar" 
                }
                </button>
            </div>
        </div>
    );
}

export default Form