
//funcion encargada de manejar las peticiones API

export const fetcher = async ({url, method, body = null}) => {

    const options = {
        method,
        headers: { 'Content-Type': 'application/json' }
    };

    if(body){
        options.body = JSON.stringify(body)
    }

    try {

        const response = await fetch(url, options);
        const responseData = await response?.json() || [];

        return responseData;
    } catch (error) {
        return `Error al realizar la solicitud: ${error.message}`;
    }

}