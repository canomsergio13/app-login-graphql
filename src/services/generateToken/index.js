
import { fetcher } from '../fetcher'

//funcion API para generar el token desde el Back con GraphQL

export const generate_Token = async ({email,password}) => {
    try {
        const data = await fetcher({
            url: '/api/generateToken',
            method: 'POST',
            body:{ email, password }
        });

        return data;
    } catch (error) {
        return error;
    }
}