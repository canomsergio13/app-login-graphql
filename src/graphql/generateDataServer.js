import { getClient }  from "../lib/server";
import { gql }  from "@apollo/client";

// fn para de GraphQL (mutation) para usar desde el servidor
export const generateTokenMutation = async ({email, password}) => {
    try {
        const { data } = await getClient().mutate({
            mutation: gql`
                mutation GenerateToken($email: String!, $password: String!) {
                    generateCustomerToken(email: $email, password: $password) {
                        token
                    }
                } 
            `,
            variables: {
                email: email,
                password: password
            }
        });

        return data?.generateCustomerToken;
    } catch (error) {
        return error?.graphQLErrors[0] || error;
    }
};
