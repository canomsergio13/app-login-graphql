

'use client'

import { ChakraProvider } from '@chakra-ui/react'

{/* Provider de Chakra */}

export const ChakraProvider__ = ({ children }) => 
    
    <ChakraProvider>
        {children}
    </ChakraProvider>