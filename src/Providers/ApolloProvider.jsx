"use client"

import { ApolloNextAppProvider } from "@apollo/experimental-nextjs-app-support/ssr"
import { makeClient } from "../lib/client"

{/* Provider de Apollo */}

export const ApolloProvider = ({children}) => 

    <ApolloNextAppProvider makeClient={makeClient} >
        {children}
    </ApolloNextAppProvider>
