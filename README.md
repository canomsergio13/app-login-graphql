# APP login - REACT NEXT.JS

## Requisitos

- Node.js
- npm

## NOTA IMPORTANTE: 

### Es importante cambiar los valores de las variables de entorno correspondiete en el archivo .env.local
### URI corresponde a la url del servicio
### STORE corresponde al valor store del headers del servicio

## Instalación

Sigue estos pasos para instalar el proyecto:

1. **Clonar el repositorio**

    Clona este repositorio a tu máquina local usando:

    ```bash
    git clone URL_DEL_REPOSITORIO
    ```

2. **Instalar las dependencias**

    Navega hasta la carpeta del proyecto y ejecuta el siguiente comando para instalar todas las dependencias necesarias:

    ```bash
    cd app-login-graphql
    npm install
    ```

3. **Configurar las variables de entorno**

    Crea un archivo `.env.local` en la raíz del proyecto y añade tus variables de entorno. Por ejemplo:

    ```bash
    URI="URL_DEL_SERVICIO_GRAPHQL"
    STORE="VALOR_STORE"
    ```

4. **Construir el proyecto**

    Ejecuta el siguiente comando para construir el proyecto:

    ```bash
    npm run build
    ```

5. **Iniciar el servidor de desarrollo**

    Finalmente, inicia el servidor de desarrollo con:

    ```bash
    npm run dev
    ```

Ahora deberías poder ver el proyecto ejecutándose en `http://localhost:3000`.

